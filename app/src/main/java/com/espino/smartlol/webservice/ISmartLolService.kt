package com.espino.smartlol.webservice

import com.espino.smartlol.models.LoggedUser
import com.espino.smartlol.models.UserSummonerCredentials
import com.espino.smartlol.models.champion.Champion
import com.espino.smartlol.models.champion.ChampionList
import com.espino.smartlol.models.match.CurrentMatch
import com.espino.smartlol.models.summoner.Summoner
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface ISmartLolService {

    @GET("summoner/{name}")
    fun getSummoner(@Path("name") name: String, @Query("platform") platform: String, @Query("language") language: Int): Call<Summoner>

    @GET("champions/")
    fun getAllChampions(): Call<ChampionList>

    @GET("champion/{id}")
    fun getChampion(@Path("id") championId: Int, @Query("language") language: Int): Call<Champion>

    @GET("currentgame/{userId}")
    fun getCurrentMatch(@Path("userId") summonerId: Long, @Query("platform") platform: String, @Query("language") language: Int): Call<CurrentMatch>

    @FormUrlEncoded
    @POST("users")
    fun createUser(@Field("username") username: String, @Field("email") email: String, @Field("password") passwd: String): Call<LoggedUser>

    @FormUrlEncoded
    @POST("summoner/{name}/credentials")
    fun getUserSummonerCredentials(@Path("name") name: String, @Query("platform") platform: String, @Field("user_id") userId: Long): Call<UserSummonerCredentials>

    @FormUrlEncoded
    @POST("users/login")
    fun authenticate(@Field("identifier") identifier: String, @Field("password") password: String): Call<LoggedUser>

    @FormUrlEncoded
    @POST("users/confirm-email/resend")
    fun resendConfirmationEmail(@Field("email") email: String): Call<ResponseBody>

    @GET("users/{username}")
    fun checkUsername(@Path("username") username: String): Call<ResponseBody>

    @FormUrlEncoded
    @POST("users/reset-password")
    fun sendResetPasswordEmail(@Field("email") email: String): Call<ResponseBody>


    companion object {
        var instance: ISmartLolService? = null
        fun create(): ISmartLolService {
            if (instance == null) {
                instance = Retrofit.Builder()
                        .baseUrl("http://app.smartlol.eu/smartLoL/")
                        .addConverterFactory(GsonConverterFactory.create())
                        .build()
                        .create(ISmartLolService::class.java)
            }

            return instance!!
        }
    }
}