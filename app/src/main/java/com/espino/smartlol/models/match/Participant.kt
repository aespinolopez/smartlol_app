package com.espino.smartlol.models.match

import com.espino.smartlol.models.champion.Champion
import com.espino.smartlol.models.summoner.SummonerLeague
import com.espino.smartlol.models.summoner.SummonerTopChampions
import io.realm.RealmList
import io.realm.RealmObject


open class Participant(
        var champion: Champion? = null,
        var league: String = "",
        var spell1: SummonerSpell? = null,
        var spell2: SummonerSpell? = null,
        var summonerId: Int = 0,
        var summoner_name: String = "",
        var team: Int = 0,
        var top_champions: RealmList<SummonerTopChampions> = RealmList(),
        var current_champion: SummonerTopChampions? = null
) : RealmObject()