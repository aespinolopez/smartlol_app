package com.espino.smartlol.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class UserSummonerCredentials(
        @PrimaryKey
        var id: Long = -1L,
        var accountId: Long = -1L,
        var name: String = "",
        var lvl: Int = 0,
        var icon: String = "",
        var validUntil: Long = 0L
): RealmObject()