package com.espino.smartlol.models.match

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class SummonerSpell(
        @PrimaryKey
        var id: Long = 0,
        var name: String = "",
        var description: String = "",
        var image: String = ""
): RealmObject()