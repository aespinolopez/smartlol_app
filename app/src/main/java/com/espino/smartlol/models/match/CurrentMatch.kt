package com.espino.smartlol.models.match

import com.espino.smartlol.models.champion.Champion
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class CurrentMatch(
      @PrimaryKey
      var gameId: Long = 0,
      var bannedChampions: RealmList<Champion> = RealmList(),
      var participants: RealmList<Participant> = RealmList(),
      var region: String = "",
      var validUntil: Long = 0
) : RealmObject()