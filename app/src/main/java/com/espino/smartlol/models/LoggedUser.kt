package com.espino.smartlol.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class LoggedUser(
        @PrimaryKey
        var id: Long = -1L,
        var username: String = "",
        var email: String = "",
        var validated: Boolean = false
) : RealmObject()