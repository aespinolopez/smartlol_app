package com.espino.smartlol.models.champion

import io.realm.RealmObject


open class ChampionSpell(
        var name: String = "",
        var description: String = "",
        var image: String = ""
) : RealmObject()