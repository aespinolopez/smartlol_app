package com.espino.smartlol.models.champion

import io.realm.RealmObject


open class ChampionSkin(
        var name: String = "",
        var image: String = ""
) : RealmObject()
