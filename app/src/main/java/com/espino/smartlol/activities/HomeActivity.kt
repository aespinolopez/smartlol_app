package com.espino.smartlol.activities

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.preference.Preference
import android.preference.PreferenceFragment
import android.support.v4.app.Fragment
import android.transition.ChangeBounds
import android.support.v4.view.GravityCompat
import android.support.v4.view.ViewCompat
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import android.widget.TextView
import com.espino.smartlol.R
import com.espino.smartlol.fragments.*
import com.espino.smartlol.models.UserSummonerCredentials
import com.espino.smartlol.utils.SmartlolPreferences
import com.espino.smartlol.utils.loadImage
import com.espino.smartlol.utils.showNetworkErrorDialog
import com.espino.smartlol.viewmodels.HomeViewModel
import com.espino.smartlol.webservice.NetworkErrorResponse
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.application_toolbar.*
import kotlinx.android.synthetic.main.navigation_header.*


class HomeActivity : AppCompatActivity(),
        SummonerInfoFragment.IFragmentCallback,
        ChampionListFragment.IFragmentCallback,
        SmartlolPreferenceFragment.OnAppPreferenceChange
{

    private var currentGameFragment: CurrentGameFragment? = null
    private var summInfoFragment: SummonerInfoFragment? = null
    private var championListFragment: ChampionListFragment? = null
    private var championFragment: ChampionFragment? = null

    private lateinit var viewmodel: HomeViewModel
    private lateinit var preferences: SmartlolPreferences
    private lateinit var summonerCredentials: UserSummonerCredentials

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        setSupportActionBar(app_toolbar)
        preferences = SmartlolPreferences(this@HomeActivity)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_navmenu)

        viewmodel = ViewModelProviders.of(this@HomeActivity).get(HomeViewModel::class.java)
        viewmodel.init(preferences.summoner, preferences.region, preferences.userId.toInt())

        if(savedInstanceState == null){
            summInfoFragment = SummonerInfoFragment.newInstance()
            supportFragmentManager.beginTransaction().add(R.id.home_container, summInfoFragment, SummonerInfoFragment.TAG).commit()
        }

        navigation_navview.setCheckedItem(R.id.navigation_menuitem_summoner)
        navigation_navview.setNavigationItemSelectedListener {
            if(!it.isChecked){
                clearBackStack()
                when(it.itemId){
                    R.id.navigation_menuitem_currentgame ->{
                        Log.v("HomeActivity", "clicked on currentgame")
                        currentGameFragment = CurrentGameFragment.newInstance()
                        supportFragmentManager.beginTransaction().replace(R.id.home_container, currentGameFragment, CurrentGameFragment.TAG).commit()
                    }
                    R.id.navigation_menuitem_summoner -> {
                        if(summInfoFragment == null)
                            summInfoFragment = SummonerInfoFragment.newInstance()
                        supportFragmentManager.beginTransaction().replace(R.id.home_container, summInfoFragment, SummonerInfoFragment.TAG).commit()
                    }
                    R.id.navigation_menuitem_champions -> {
                        if(championListFragment == null){
                            championListFragment = ChampionListFragment.newInstance()
                        }
                        supportFragmentManager.beginTransaction().replace(R.id.home_container, championListFragment, ChampionListFragment.TAG).commit()
                    }
                    R.id.navigation_menuitem_settings -> {
                        val fragment = SmartlolPreferenceFragment()
                        supportFragmentManager.beginTransaction().replace(R.id.home_container, fragment, null).commit()
                    }
                    R.id.navigation_menuitem_about -> {
                        val fragment = AboutSmartlolFragment()
                        supportFragmentManager.beginTransaction().replace(R.id.home_container, fragment, null).commit()
                    }
                    else->Log.v("HomeActivity:", "onNavigationItemSelected: ${it.title}")
                }
            }

            navigation_drawerlayout.closeDrawers()

            true
        }
    }

    override fun onStart() {
        super.onStart()

        viewmodel.data?.observe(this@HomeActivity, Observer {
            if(it?.count() == 1){
                summonerCredentials = it.first()!!
                setSummonerData(summonerCredentials)
            }
        })

        viewmodel.networkError?.observe(this@HomeActivity, Observer {
            if(it != null){
                showNetworkErrorDialog(it)
                preferences.summoner = viewmodel.oldSummoner
                viewmodel.data = viewmodel.oldData
                viewmodel.networkError?.value = null
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.app_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            android.R.id.home->navigation_drawerlayout.openDrawer(GravityCompat.START)
            R.id.menu_logout-> logOut()
        }

        return true
    }

    //TODO animation doesn't work
    override fun loadData(args: Bundle) {
        ViewCompat.setTransitionName(findViewById(R.id.linear_search), "layout_search")
        ViewCompat.setTransitionName(findViewById(R.id.summinfo_txi_search), "txi_search")
        ViewCompat.setTransitionName(findViewById(R.id.summinfo_btn_search), "btn_search")

        summInfoFragment = SummonerInfoFragment.newInstance(args)
        summInfoFragment?.sharedElementEnterTransition = ChangeBounds()

        supportFragmentManager.beginTransaction()
                .addSharedElement(findViewById(R.id.linear_search), "layout_search")
                .addSharedElement(findViewById(R.id.summinfo_txi_search), "txi_search")
                .addSharedElement(findViewById(R.id.summinfo_btn_search), "btn_search")
                .replace(R.id.home_container, summInfoFragment, SummonerInfoFragment.TAG)
                .commit()
    }

    override fun loadChampionData(args: Bundle) {
        championFragment = ChampionFragment.newInstance(args)

        supportFragmentManager.beginTransaction()
                .replace(R.id.home_container, championFragment, ChampionFragment.TAG)
                .addToBackStack(ChampionInfoFragment.TAG)
                .commit()
    }

    override fun onLanguageChange() {
        viewmodel.updateLanguage()
    }

    override fun onSummonerChanged(oldSummoner: String) {
        viewmodel.updateCredentials(preferences.summoner!!, preferences.region!!, preferences.userId.toInt(), oldSummoner)
        viewmodel.data?.observe(this@HomeActivity, Observer {
            if(it?.count() == 1){
                summonerCredentials = it.first()!!
                setSummonerData(summonerCredentials)
                preferences.summoner = summonerCredentials.name
                preferences.riotId = summonerCredentials.id
                preferences.riotAccountId = summonerCredentials.accountId
            }
        })
    }

    fun getChampionFragment() : Fragment? = championFragment

    private fun clearBackStack() {
        if(supportFragmentManager.backStackEntryCount > 0){
            supportFragmentManager.popBackStackImmediate()
        }
    }

    private fun setSummonerData(summoner: UserSummonerCredentials) {
        if(navheader_txv_summonername == null){
            val header = navigation_navview.getHeaderView(0)
            val region = header.findViewById(R.id.navheader_txv_region) as TextView
            val name = header.findViewById(R.id.navheader_txv_summonername) as TextView
            val img = header.findViewById(R.id.navheader_img_icon) as ImageView

            name.text = summoner.name
            region.text = preferences.region
            loadImage(img, summoner.icon)
        }else{
            loadImage(navheader_img_icon, summoner.icon)
            navheader_txv_summonername.text = summoner.name
            navheader_txv_region.text = preferences.region
        }

    }

    private fun logOut() {
        preferences.userId = -1
        preferences.userEmail = null
        preferences.isValidatedEmail = false
        preferences.summoner = null
        preferences.riotId = -1
        preferences.riotAccountId = -1

        finish()
    }
}
