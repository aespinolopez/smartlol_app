package com.espino.smartlol.activities

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.Toast
import com.espino.smartlol.R
import com.espino.smartlol.interfaces.ILogin
import com.espino.smartlol.models.LoggedUser
import com.espino.smartlol.utils.SmartlolPreferences
import com.espino.smartlol.presenters.LoginPresenterImpl
import com.espino.smartlol.utils.showNetworkErrorDialog
import com.espino.smartlol.webservice.NetworkErrorResponse
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity(), ILogin.View{

    companion object {
        val TAG: String = "login"
        val REQUEST_SIGNUP: Int = 0
    }

    private lateinit var presenter: ILogin.Presenter
    private lateinit var preferences: SmartlolPreferences

    private lateinit var edt_email: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferences = SmartlolPreferences(this@LoginActivity)

        if(preferences.userId != -1L && preferences.userEmail != null && preferences.isValidatedEmail){
          onAuthenticationSuccess()
        }
        setContentView(R.layout.activity_login)
        presenter = LoginPresenterImpl(this@LoginActivity)

        activitylogin_btn_login.setOnClickListener {
            val identifier = activitylogin_edt_identifier.text.toString()
            val password = activitylogin_edt_password.text.toString()
            if(identifier.isNotBlank() && password.isNotBlank()){
                toggleVisibility()
                presenter.authenticate(identifier, password)
            }
        }

        activitylogin_txv_signup.setOnClickListener {
            val intent = Intent(this@LoginActivity, SignUpActivity::class.java)
            startActivityForResult(intent, REQUEST_SIGNUP)
        }

        activitylogin_txv_forgotpasswd.setOnClickListener {
            showSendResetPasswordEmailDialog()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(requestCode == REQUEST_SIGNUP){
            if(resultCode == Activity.RESULT_OK){
                val userId: Long = data!!.getLongExtra(SignUpActivity.FIELD_ID, -1L)
                val username: String = data.getStringExtra(SignUpActivity.FIELD_USERNAME)
                val email: String = data.getStringExtra(SignUpActivity.FIELD_EMAIL)
                val loggedUser = LoggedUser(userId, username, email)

                if(userId != -1L){
                    presenter.saveLoggedUser(loggedUser)
                    startActivity(Intent(this@LoginActivity, SetupActivity::class.java))
                    finish()
                }
            }
        }
    }

    override fun showNetworkError(error: NetworkErrorResponse) {
        showNetworkErrorDialog(error)
    }

    override fun onAuthenticationSuccess() {
        if(preferences.isValidatedEmail){
            startActivity(Intent(this@LoginActivity, SetupActivity::class.java))
            finish()
        }else{
            showNotValidateEmailDialog()
            toggleVisibility()
        }

    }

    override fun saveLoggedState(id: Long, email: String, validated: Boolean){
        preferences.userId = id
        preferences.userEmail = email
        preferences.isValidatedEmail = validated
    }

    override fun toggleVisibility() {
        activitylogin_btn_login.visibility = if(activitylogin_btn_login.visibility == View.VISIBLE) View.GONE else View.VISIBLE
        activitylogin_progressbar.visibility = if(activitylogin_btn_login.visibility == View.GONE) View.VISIBLE else View.GONE
    }

    private fun showNotValidateEmailDialog() {
        val dialog: AlertDialog = AlertDialog.Builder(this)
                .setTitle(resources.getString(R.string.not_validated_email_title))
                .setMessage(resources.getString(R.string.not_validated_email_message))
                .setNegativeButton(this.resources?.getString(android.R.string.ok), null)
                .setPositiveButton(resources.getString(R.string.resend_email), {_,_->
                    presenter.resendConfirmationEmail(preferences.userEmail!!)
                })
                .create()

        dialog.show()
    }

    private fun showSendResetPasswordEmailDialog() {

        edt_email = EditText(this@LoginActivity)
        edt_email.hint = resources.getString(R.string.email_text)
        edt_email.setTextColor(ContextCompat.getColor(this@LoginActivity, R.color.colorPrimary))

        val dialog: AlertDialog = AlertDialog.Builder(this)
                .setTitle(this.resources?.getString(R.string.reset_password_title))
                .setMessage(this.resources?.getString(R.string.reset_password_message))//
                .setView(edt_email)
                .setNegativeButton(this.resources?.getString(android.R.string.cancel), null)
                .setPositiveButton(this.resources?.getString(R.string.reset_password_send), DialogInterface.OnClickListener { _, _ ->
                    val email = edt_email.text.toString()
                    if(presenter.validateEmail(email)){
                        presenter.sendResetPasswordEmail(email)
                    }
                })
                .create()

        dialog.show()
    }

    override fun showValidationError(message: String) {
        Toast.makeText(this@LoginActivity, message, Toast.LENGTH_SHORT).show()
    }

    override fun getViewContext(): Context {
        return this@LoginActivity
    }
}