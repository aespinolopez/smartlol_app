package com.espino.smartlol.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.espino.smartlol.R
import com.espino.smartlol.interfaces.ISetup
import com.espino.smartlol.presenters.SetupPresenterImpl
import com.espino.smartlol.utils.SmartlolPreferences
import com.espino.smartlol.utils.showNetworkErrorDialog
import com.espino.smartlol.webservice.NetworkErrorResponse
import kotlinx.android.synthetic.main.activity_setup.*


class SetupActivity : AppCompatActivity(), ISetup.View {

    companion object {
        const val TAG: String = "setup_activity"
    }

    private lateinit var presenter: ISetup.Presenter
    private lateinit var preferences: SmartlolPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        preferences = SmartlolPreferences(this@SetupActivity)

        Log.v("user_id", "${preferences.userId}")
        if(preferences.riotId != -1L && preferences.riotAccountId != -1L && preferences.summoner != null && preferences.region != null){
            onSetupComplete()
        }
        setContentView(R.layout.activity_setup)

        presenter = SetupPresenterImpl(this@SetupActivity)

        activitysetup_btn_setup.setOnClickListener {
            val summonername = activitysetup_edt_summoner.text.toString()
            val region = activity_setup_spn_region.getSelectedRegion()
            if(summonername.isNotBlank()){
                toggleVisibility()
                presenter.registerSummoner(summonername, region, preferences.userId)
            }
        }
    }

    override fun showNetworkError(error: NetworkErrorResponse) {
        showNetworkErrorDialog(error)
    }

    override fun saveLoggedSummonerPreferences(riotId: Long, riotAccountId: Long, summonername: String) {
        preferences.riotId = riotId
        preferences.riotAccountId = riotAccountId
        preferences.summoner = summonername
        preferences.region = activity_setup_spn_region.getSelectedRegion()
    }

    override fun onSetupComplete() {
        startActivity(Intent(this@SetupActivity, HomeActivity::class.java))
        finish()
    }

    override fun toggleVisibility() {
        activitysetup_btn_setup.visibility = if(activitysetup_btn_setup.visibility == View.VISIBLE) View.GONE else View.VISIBLE
        activitysetup_progressbar.visibility = if(activitysetup_progressbar.visibility == View.GONE) View.VISIBLE else View.GONE
    }
}