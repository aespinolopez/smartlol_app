package com.espino.smartlol.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.espino.smartlol.R
import com.espino.smartlol.fragments.SignUpMethodsFragment
import com.espino.smartlol.fragments.SignUpWithEmailFragment
import com.espino.smartlol.models.LoggedUser

class SignUpActivity : AppCompatActivity(), SignUpMethodsFragment.ISignUpMethod, SignUpWithEmailFragment.OnSignUpCallback {

    companion object {
        const val TAG: String = "SIGN_UP"
        const val FIELD_ID = "id"
        const val FIELD_USERNAME = "username"
        const val FIELD_EMAIL = "email"
    }

    private var signupFragment: SignUpWithEmailFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signup)

        if(savedInstanceState == null){
            signupFragment = SignUpWithEmailFragment.newInstance()
            supportFragmentManager.beginTransaction().replace(R.id.signup_container, signupFragment, SignUpWithEmailFragment.TAG).addToBackStack(TAG).commit()
        }
    }

    override fun onEmailChoose() {
        if(signupFragment == null){
            signupFragment = SignUpWithEmailFragment.newInstance()
        }

        supportFragmentManager.beginTransaction().replace(R.id.signup_container, signupFragment, SignUpWithEmailFragment.TAG).addToBackStack(TAG).commit()
    }

    override fun onGoogleChoose() {
        Log.v(TAG, "Google clicked")
    }

    override fun onSignUp(id: Long, username: String, email: String) {
        val intent = Intent()

        intent.putExtra(FIELD_ID, id)
        intent.putExtra(FIELD_USERNAME, username)
        intent.putExtra(FIELD_EMAIL, email)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    override fun onCancel() {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }
}