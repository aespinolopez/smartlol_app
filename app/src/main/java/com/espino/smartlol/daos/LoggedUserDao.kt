package com.espino.smartlol.daos

import com.espino.smartlol.models.LoggedUser
import io.realm.Realm
import io.realm.RealmAsyncTask


class LoggedUserDao(private val dbInstance: Realm) {

    fun get(id: Int): LoggedUser {
        return dbInstance.where(LoggedUser::class.java)
                .equalTo("id", id)
                .findFirst()!!
    }

    fun save(user: LoggedUser): RealmAsyncTask{
        return dbInstance.executeTransactionAsync {
            it.insertOrUpdate(user)
        }
    }

    fun delete(user: LoggedUser): RealmAsyncTask{
        return dbInstance.executeTransactionAsync {
            user.deleteFromRealm()
        }
    }


}