package com.espino.smartlol.daos

import com.espino.smartlol.models.UserSummonerCredentials
import com.espino.smartlol.utils.LiveRealmData
import com.espino.smartlol.utils.asLiveData
import io.realm.Case
import io.realm.Realm
import java.util.*


class UserSummonerCredentialsDao(override var dbInstance: Realm) : AbstractDao<UserSummonerCredentials>() {

    override val FRESH_TIMEOUT = 1 //day

    override fun getData(identifier: String?, region: String?): LiveRealmData<UserSummonerCredentials> {
        return dbInstance.where(UserSummonerCredentials::class.java)
                .equalTo("name", identifier, Case.INSENSITIVE)
                .findAllAsync()
                .asLiveData()
    }

    override fun save(element: UserSummonerCredentials, dbInstance: Realm) {
        val calendar: Calendar = Calendar.getInstance()
        calendar.add(Calendar.DAY_OF_MONTH, FRESH_TIMEOUT)
        element.validUntil = calendar.timeInMillis

        dbInstance.executeTransactionAsync {
            it.copyToRealmOrUpdate(element)
        }

    }

    override fun hasElement(dbInstance: Realm, identifier: String?, region: String?): Boolean {
        var isValidData = false

        val summoner: UserSummonerCredentials? = dbInstance.where(UserSummonerCredentials::class.java)
                .equalTo("name", identifier, Case.INSENSITIVE)
                .findFirst()

        if (summoner != null) {
            val instant = Calendar.getInstance()
            val expirationTime = Calendar.getInstance()
            expirationTime.timeInMillis = summoner.validUntil

            if (instant < expirationTime)
                isValidData = true

        }

        return isValidData
    }

}

