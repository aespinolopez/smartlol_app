package com.espino.smartlol.daos

import com.espino.smartlol.models.match.CurrentMatch
import com.espino.smartlol.utils.LiveRealmData
import com.espino.smartlol.utils.asLiveData
import io.realm.Realm
import java.util.Calendar


class CurrentGameDao(override val dbInstance: Realm) : AbstractDao<CurrentMatch>(){
    override val FRESH_TIMEOUT: Int = 15 //minutes

    override fun getData(identifier: String?, region: String?): LiveRealmData<CurrentMatch> {
        return dbInstance.where(CurrentMatch::class.java)
                .findAllAsync()
                .asLiveData()
    }

    override fun save(element: CurrentMatch, dbInstance: Realm) {
        val calendar: Calendar = Calendar.getInstance()
        calendar.add(Calendar.MINUTE, FRESH_TIMEOUT)
        element.validUntil = calendar.timeInMillis

        dbInstance.executeTransactionAsync {
            it.delete(CurrentMatch::class.java)
            it.copyToRealmOrUpdate(element)
        }
    }

    override fun hasElement(dbInstance: Realm, identifier: String?, region: String?): Boolean {
        var isValidData = false

        val currentMatch: CurrentMatch? = dbInstance.where(CurrentMatch::class.java)
                .findFirst()

        if(currentMatch != null){
            val instant = Calendar.getInstance()
            val expirationTime = Calendar.getInstance()
            expirationTime.timeInMillis = currentMatch.validUntil

            if(instant < expirationTime){
                isValidData = true
            }
        }

        return isValidData
    }

}