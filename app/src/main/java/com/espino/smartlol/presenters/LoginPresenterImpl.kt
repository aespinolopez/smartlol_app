package com.espino.smartlol.presenters

import android.util.Log
import com.espino.smartlol.R
import com.espino.smartlol.interfaces.ILogin
import com.espino.smartlol.models.LoggedUser
import com.espino.smartlol.utils.loggedUserDao
import com.espino.smartlol.webservice.ISmartLolService
import com.espino.smartlol.webservice.NetworkErrorResponse
import io.realm.Realm
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import java.io.IOException


class LoginPresenterImpl(private val view: ILogin.View) : ILogin.Presenter {

    private val EMAIL_REGEX = Regex("\\w+[\\w._-]*@\\w+.\\w+")

    private val webService: ISmartLolService = ISmartLolService.create()
    private val dbInstance: Realm = Realm.getDefaultInstance()

    override fun authenticate(identifier: String, password: String) {
        async(UI) {
            var user: LoggedUser? = null
            val response = bg {
                try {
                    val serviceResponse = webService.authenticate(identifier, password).execute()
                    if(serviceResponse.code() == 200){
                        user = serviceResponse.body()!!
                        NetworkErrorResponse(0)
                    }else{
                        NetworkErrorResponse(serviceResponse.code(), serviceResponse.headers(), serviceResponse.errorBody()?.string())
                    }
                } catch (ex: IOException) {
                    Log.e("Login Coroutine", ex.message)
                    NetworkErrorResponse(1)
                }
            }

            val result = response.await()
            if(result.statusCode >= 400 || result.statusCode == 1){
                view.toggleVisibility()
                view.showNetworkError(result)
            }else{
                saveLoggedUser(user!!)
            }
        }
    }

    override fun resendConfirmationEmail(email: String) {
        async(UI) {
            val response = bg {
                try{
                    val serviceResponse = webService.resendConfirmationEmail(email).execute()
                    if(serviceResponse.code() == 204){
                        NetworkErrorResponse(0)
                    }else{
                        NetworkErrorResponse(serviceResponse.code(), serviceResponse.headers(), serviceResponse.errorBody()?.string())
                    }
                }catch (ex: IOException){
                    Log.e("Login Coroutine", ex.message)
                    NetworkErrorResponse(1)
                }
            }

            val result = response.await()
            if(result.statusCode >= 400 || result.statusCode == 1){
                view.showNetworkError(result)
            }
        }
    }

    override fun sendResetPasswordEmail(email: String) {
        async(UI) {
            val response = bg {
                try{
                    val serviceResponse = webService.sendResetPasswordEmail(email).execute()
                    if(serviceResponse.code() == 204){
                        NetworkErrorResponse(0)
                    }else{
                        NetworkErrorResponse(serviceResponse.code(), serviceResponse.headers(), serviceResponse.errorBody()?.string())
                    }
                }catch (ex: IOException){
                    Log.e("Login Coroutine", ex.message)
                    NetworkErrorResponse(1)
                }
            }

            val result = response.await()
            if(result.statusCode >= 400 || result.statusCode == 1){
                view.showNetworkError(result)
            }
        }
    }

    override fun saveLoggedUser(user: LoggedUser) {
        //first lambda is the task, second onSuccess callback
        dbInstance.executeTransactionAsync(
                {
                    it.insertOrUpdate(user)
                },
                { ->
                    dbInstance.close()
                    view.saveLoggedState(user.id, user.email, user.validated)
                    view.onAuthenticationSuccess()
                }
        )
    }

    override fun validateEmail(email: String): Boolean {
        if(email.matches(EMAIL_REGEX)){
            return true
        }else{
            view.showValidationError(view.getViewContext().resources.getString(R.string.must_be_email))
            return false
        }
    }
}