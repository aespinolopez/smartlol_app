package com.espino.smartlol.presenters

import android.util.Log
import com.espino.smartlol.R
import com.espino.smartlol.interfaces.ISignUp
import com.espino.smartlol.webservice.ISmartLolService
import com.espino.smartlol.webservice.NetworkErrorResponse
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import java.io.IOException


class SignUpPresenterImpl(private val view: ISignUp.View) : ISignUp.Presenter {

    private val EMAIL_REGEX = Regex("\\w+[\\w._-]*@\\w+\\.\\w+")
    private val SPECIAL_CHAR_REGEX = Regex("[-.@_\\s#\$=/*]+")
    private val USERNAME_ALLOW_CHARS_REGEX = Regex("\\w*[\\w_-]+\\w*")
    private val USERNAME_LENGTH = arrayOf(2, 16)
    private val PASSWORD_LENGTH = arrayOf(6, 20)

    private val webservice: ISmartLolService = ISmartLolService.create()


    override fun signUp(username: String, email: String, passwd: String): Boolean {
        var success = false
        if(validate(username, email, passwd)){
            view.toggleVisibility()
            async(UI){
                val response = bg {
                    try {
                        val serviceResponse = webservice.createUser(username, email, passwd).execute()
                        Log.v("StatusCode", serviceResponse.code().toString())
                        if(serviceResponse.code() == 200){
                            val user = serviceResponse.body()!!
                            view.onSignUp(user.id, user.username, user.email)
                            success  = true
                            NetworkErrorResponse(0)
                        }else{
                            NetworkErrorResponse(serviceResponse.code(), serviceResponse.headers(), serviceResponse.errorBody()?.string())
                        }

                    } catch (ex: IOException) {
                        Log.e("Coroutine signupemail", ex.message)
                        NetworkErrorResponse(1)
                    }
                }
                val result = response.await()
                if(result.statusCode >= 400 || result.statusCode == 1){
                    view.toggleVisibility()
                    view.showNetworkError(result)
                }
            }
        }

        return success
    }

    private fun validate(username: String, email: String, passwd: String): Boolean {
        return validateUsername(username) && validateEmail(email) && validatePassword(passwd)
    }

    override fun checkUsername(username: String) {
        async(UI){
            val response = bg {
                try{
                    val response = webservice.checkUsername(username).execute()
                    NetworkErrorResponse(response.code(), response.headers(), response.errorBody()?.string())
                }catch (ex: Exception){
                    Log.e("Coroutine signupemail", ex.message)
                    NetworkErrorResponse(1)
                }
            }


            val result = response.await()
            if(result.statusCode == 409){
                view.showNetworkError(result)
            }
        }
    }

    override fun validateUsername(username: String): Boolean {
        if(username.length in USERNAME_LENGTH[0]..USERNAME_LENGTH[1]){
            if(username.matches(USERNAME_ALLOW_CHARS_REGEX)){
                return true
            }else{
                view.showValidationError(R.id.signup_edt_username, view.getViewContext().resources.getString(R.string.not_valid_username))
                return false
            }
        }else{
            view.showValidationError(R.id.signup_edt_username, view.getViewContext().resources.getString(R.string.must_have_length).format(USERNAME_LENGTH[0], USERNAME_LENGTH[1]))
            return false
        }
    }

    override fun validateEmail(email: String): Boolean {
        if(email.matches(EMAIL_REGEX)){
            return true
        }else{
            view.showValidationError(R.id.signup_edt_email, view.getViewContext().resources.getString(R.string.must_be_email))
            return false
        }
    }

    override fun validatePassword(passwd: String): Boolean {
        if(passwd.length !in PASSWORD_LENGTH[0]..PASSWORD_LENGTH[1]){
            view.showValidationError(R.id.signup_edt_password, view.getViewContext().resources.getString(R.string.must_have_length).format(PASSWORD_LENGTH[0], PASSWORD_LENGTH[1]))
            return false
        }

        if(!hasDigit(passwd)){
            view.showValidationError(R.id.signup_edt_password, view.getViewContext().resources.getString(R.string.must_have_digit))
            return false
        }
        if(!hasLowerCase(passwd)){
            view.showValidationError(R.id.signup_edt_password, view.getViewContext().resources.getString(R.string.must_have_lowercase))
            return false
        }
        if(!hasUpperCase(passwd)){
            view.showValidationError(R.id.signup_edt_password, view.getViewContext().resources.getString(R.string.must_have_uppercase))
            return false
        }
        if(!hasSpecialChar(passwd)){
            view.showValidationError(R.id.signup_edt_password, view.getViewContext().resources.getString(R.string.must_have_specialchar))
            return false
        }

        return true
    }

    private fun hasDigit(passwd: String): Boolean {
        val regex = Regex("\\d")
        return regex.containsMatchIn(passwd)
    }

    private fun hasUpperCase(passwd: String): Boolean {
        val regex = Regex("[A-Z]+")
        return regex.containsMatchIn(passwd)
    }

    private fun hasLowerCase(passwd: String): Boolean {
        val regex = Regex("[a-z]+")
        return regex.containsMatchIn(passwd)
    }

    private fun hasSpecialChar(passwd: String): Boolean {
        return SPECIAL_CHAR_REGEX.containsMatchIn(passwd)

    }

}