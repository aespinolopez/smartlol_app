package com.espino.smartlol.presenters

import android.util.Log
import com.espino.smartlol.interfaces.ISetup
import com.espino.smartlol.models.UserSummonerCredentials
import com.espino.smartlol.webservice.ISmartLolService
import com.espino.smartlol.webservice.NetworkErrorResponse
import io.realm.Realm
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import java.io.IOException
import java.util.*


class SetupPresenterImpl(private val view: ISetup.View) : ISetup.Presenter {

    private val webService = ISmartLolService.create()
    private val dbInstance = Realm.getDefaultInstance()

    override fun registerSummoner(summonername: String, region: String, userId: Long) {
        async(UI){
            var userSummonerCredentials: UserSummonerCredentials? = null
            val response = bg {
                try {
                    val serviceResponse = webService.getUserSummonerCredentials(summonername, region, userId).execute()
                    if(serviceResponse.code() == 200){
                        userSummonerCredentials = serviceResponse.body()!!
                        NetworkErrorResponse(0)
                    }else{
                        NetworkErrorResponse(serviceResponse.code(), serviceResponse.headers(), serviceResponse.errorBody()?.string())
                    }
                } catch (ex: IOException) {
                    Log.e("SetUp Coroutine", ex.message)
                    NetworkErrorResponse(1)
                }
            }

            val result = response.await()
            if(result.statusCode >= 400 || result.statusCode == 1){
                view.toggleVisibility()
                view.showNetworkError(result)
            }else{
                saveLoggedSummoner(userSummonerCredentials!!)
            }
        }
    }

    override fun saveLoggedSummoner(summoner: UserSummonerCredentials) {
        //first lambda is the task, second onSuccess callback
        dbInstance.executeTransactionAsync(
                {
                    val instant = Calendar.getInstance()
                    instant.add(Calendar.DAY_OF_MONTH, 1)
                    summoner.validUntil = instant.timeInMillis
                    it.insertOrUpdate(summoner)
                },
                {->
                    view.saveLoggedSummonerPreferences(summoner.id, summoner.accountId, summoner.name)
                    dbInstance.close()
                    view.onSetupComplete()
                })
    }
}