package com.espino.smartlol.repositories

import com.espino.smartlol.daos.CurrentGameDao
import com.espino.smartlol.models.match.CurrentMatch
import com.espino.smartlol.utils.currentGameDao
import com.espino.smartlol.webservice.NetworkErrorResponse
import io.realm.Realm
import java.io.IOException


class CurrentGameRepository(dbInstance: Realm) : AbstractRepository<CurrentMatch>(){
    override val dao: CurrentGameDao = dbInstance.currentGameDao()

    override fun refreshData(identifier: String?, region: String?, language: Int): NetworkErrorResponse? {
        var errorResponse: NetworkErrorResponse? = null
        Realm.getDefaultInstance().use{
            if(!dao.hasElement(it, identifier=identifier, region=region!!)){
                try{
                    val response = service.getCurrentMatch(identifier!!.toLong(), region, language).execute()
                    if(response.code() == 200){
                        val currentMatch: CurrentMatch = response.body()!!
                        currentMatch.region = region
                        dao.save(currentMatch, it)
                    } else{
                        errorResponse = NetworkErrorResponse(response.code(), response.headers(), response.errorBody()?.string())
                    }
                }catch (ex: IOException){
                    errorResponse = NetworkErrorResponse(1)
                }
            }
        }

        return errorResponse
    }

}