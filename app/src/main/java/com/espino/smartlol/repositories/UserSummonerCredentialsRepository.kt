package com.espino.smartlol.repositories

import com.espino.smartlol.daos.UserSummonerCredentialsDao
import com.espino.smartlol.models.UserSummonerCredentials
import com.espino.smartlol.models.summoner.Summoner
import com.espino.smartlol.utils.userSummonerCredentialsDao
import com.espino.smartlol.webservice.NetworkErrorResponse
import io.realm.Realm
import java.io.IOException


class UserSummonerCredentialsRepository(dbInstance: Realm): AbstractRepository<UserSummonerCredentials>() {
    override val dao: UserSummonerCredentialsDao = dbInstance.userSummonerCredentialsDao()

     override fun refreshData(identifier: String?, region: String?, userId: Int): NetworkErrorResponse?{
        var errorResponse: NetworkErrorResponse? = null
        Realm.getDefaultInstance().use {
            if (!dao.hasElement(it, identifier = identifier, region = region!!)) {
                try {
                    val response = service.getUserSummonerCredentials(identifier!!, region, userId.toLong()).execute()
                    if (response.code() == 200) {
                        val summoner: UserSummonerCredentials = response.body()!!
                        dao.save(summoner, it)
                    } else {
                        errorResponse = NetworkErrorResponse(response.code(), response.headers(), response.errorBody()?.string())
                    }
                } catch (ex: IOException) {
                    errorResponse = NetworkErrorResponse(1)
                }
            }
        }
        return errorResponse
    }

}