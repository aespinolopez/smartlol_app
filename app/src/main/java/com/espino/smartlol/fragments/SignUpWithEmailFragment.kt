package com.espino.smartlol.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.espino.smartlol.R
import com.espino.smartlol.interfaces.ISignUp
import com.espino.smartlol.presenters.SignUpPresenterImpl
import com.espino.smartlol.utils.showNetworkErrorDialog
import com.espino.smartlol.webservice.NetworkErrorResponse
import kotlinx.android.synthetic.main.fragment_signup_email.*


class SignUpWithEmailFragment : Fragment(), ISignUp.View{

    interface OnSignUpCallback {
        fun onSignUp(id: Long, username: String, email: String)
        fun onCancel()
    }

    companion object {
        val TAG: String = "signup_with_email"
        fun newInstance(args: Bundle? = null): SignUpWithEmailFragment{
            val fragment = SignUpWithEmailFragment()
            fragment.arguments = args

            return fragment
        }
    }

    private lateinit var callback: OnSignUpCallback
    private lateinit var presenter: ISignUp.Presenter

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try{
            callback = activity as OnSignUpCallback
        }catch (ex: ClassCastException){
            Log.e("Activity_error", "activity ${activity!!.javaClass.simpleName} must implement interface SignUpMethodsFragment.ISignUpMethod")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_signup_email, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter = SignUpPresenterImpl(this@SignUpWithEmailFragment)
        setTextListeners()

        signup_edt_username.setOnFocusChangeListener { _: View, focus: Boolean ->
            val username = signup_edt_username.text.toString()
            if(!focus && presenter.validateUsername(username)){
                presenter.checkUsername(username)
            }
        }

        signupemail_btn_signup.setOnClickListener {
            val username = signup_edt_username.text.toString()
            val email = signup_edt_email.text.toString()
            val passwd = signup_edt_password.text.toString()

            presenter.signUp(username, email, passwd)
        }

        signupemail_txv_alreadymember.setOnClickListener {
            callback.onCancel()
        }


    }

    override fun showValidationError(viewId: Int, message: String) {
        when(viewId) {
            R.id.signup_edt_username -> {
                signup_edt_username.error = message
            }
            R.id.signup_edt_email -> {
                signup_edt_email.error = message
            }
            R.id.signup_edt_password -> {
                signup_edt_password.error = message
            }
        }
    }

    private fun setTextListeners(){

        signup_edt_username.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                if(presenter.validateUsername(p0.toString())){
                    signup_edt_username.error = null
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        })

        signup_edt_email.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                if(presenter.validateEmail(p0.toString())){
                    signup_edt_email.error = null
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        })

        signup_edt_password.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {
                if(presenter.validatePassword(p0.toString())){
                    signup_edt_password.error = null
                }
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}

        })
    }

    override fun getViewContext(): Context = context!!

    override fun showNetworkError(error: NetworkErrorResponse) {
        showNetworkErrorDialog(error)
    }


    override fun onSignUp(id: Long, username: String, email: String) {
        callback.onSignUp(id, username, email)
    }

    override fun toggleVisibility() {
        signupemail_btn_signup.visibility = if(signupemail_btn_signup.visibility == View.VISIBLE) View.GONE else View.VISIBLE
        signupemail_progressbar.visibility = if(signupemail_progressbar.visibility == View.GONE) View.VISIBLE else View.GONE
    }
}