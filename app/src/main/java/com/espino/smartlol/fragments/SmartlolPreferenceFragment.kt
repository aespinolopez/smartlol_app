package com.espino.smartlol.fragments

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.preference.PreferenceFragmentCompat
import android.util.Log
import com.espino.smartlol.R
import com.espino.smartlol.utils.SmartlolPreferences


class SmartlolPreferenceFragment : PreferenceFragmentCompat(), SharedPreferences.OnSharedPreferenceChangeListener {

    private lateinit var callback: OnAppPreferenceChange
    private lateinit var appPreferences: SmartlolPreferences

    private lateinit var oldSummoner: String

    interface OnAppPreferenceChange {
        fun onLanguageChange()
        fun onSummonerChanged(oldSummoner: String)
    }

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        setPreferencesFromResource(R.xml.preferences, null)
    }

    override fun onSharedPreferenceChanged(preference: SharedPreferences?, key: String?) {
        when(key) {
            "selected_language" -> callback.onLanguageChange()
            "summoner_name" -> callback.onSummonerChanged(oldSummoner)
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try{
            callback = activity as OnAppPreferenceChange
        }catch(ex: ClassCastException){
            Log.e("Activity_error", "activity ${activity!!.javaClass.simpleName} must implement interface SmartlolPreferenceFragment.OnAppPreferenceChange")
        }

        appPreferences = SmartlolPreferences(context!!)
        oldSummoner = appPreferences.summoner!!
    }

    override fun onResume() {
        super.onResume()
        preferenceManager.sharedPreferences.registerOnSharedPreferenceChangeListener(this@SmartlolPreferenceFragment)
    }

    override fun onPause() {
        preferenceManager.sharedPreferences.unregisterOnSharedPreferenceChangeListener(this@SmartlolPreferenceFragment)
        super.onPause()
    }

}