package com.espino.smartlol.fragments

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.espino.smartlol.R
import com.espino.smartlol.adapters.CurrentGameAdapter
import com.espino.smartlol.databinding.FragmentCurrentgameBinding
import com.espino.smartlol.utils.SmartlolPreferences
import com.espino.smartlol.utils.getLanguage
import com.espino.smartlol.utils.showNetworkErrorDialog
import com.espino.smartlol.viewmodels.CurrentGameViewModel
import kotlinx.android.synthetic.main.fragment_currentgame.*


class CurrentGameFragment : Fragment(){

    private lateinit var binder: FragmentCurrentgameBinding
    private lateinit var viewmodel: CurrentGameViewModel
    private lateinit var adapter: CurrentGameAdapter
    private lateinit var preferences: SmartlolPreferences

    companion object {
        val TAG: String = "current_game"

        fun newInstance(args: Bundle?=null): CurrentGameFragment{
            val fragment = CurrentGameFragment()
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binder = DataBindingUtil.inflate(inflater, R.layout.fragment_currentgame, container, false)

        return binder.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        adapter = CurrentGameAdapter {
            val args = Bundle()
            val enemyTips: ArrayList<String> = it.champion?.enemy_tips?.toList() as ArrayList<String>
            args.putStringArrayList(ChampionBaseTipsFragment.ENEMY_TIPS, enemyTips)
            val allyTips: ArrayList<String> = it.champion?.ally_tips?.toList() as ArrayList<String>
            args.putStringArrayList(ChampionBaseTipsFragment.ALLY_TIPS, allyTips)

            fragmentManager?.beginTransaction()?.add(ChampionBaseTipsFragment.newInstance(args), ChampionBaseTipsFragment.TAG)?.commit()
        }
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        val itemSeparator = DividerItemDecoration(currentgame_rcv.context, layoutManager.orientation)
        currentgame_rcv.adapter = adapter
        currentgame_rcv.layoutManager = layoutManager
        currentgame_rcv.addItemDecoration(itemSeparator)

        viewmodel = ViewModelProviders.of(this@CurrentGameFragment).get(CurrentGameViewModel::class.java)

        if(viewmodel.data != null) {
            toggleVisibility()
            showContent()
            val currentMatch = viewmodel.getData()
            adapter.participants = currentMatch?.participants
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        preferences = SmartlolPreferences(context!!)

        currentgame_btn_fetch.setOnClickListener {
            toggleVisibility()
            viewmodel.init(preferences.riotId.toString(), preferences.region, getLanguage())
            subscribe()
        }


    }

    private fun toggleVisibility() {
        currentgame_progressbar.visibility = if(currentgame_progressbar.visibility == View.GONE) View.VISIBLE else View.GONE
        currentgame_txv_empty.visibility = if(currentgame_txv_empty.visibility == View.VISIBLE) View.GONE else View.VISIBLE
        currentgame_btn_fetch.visibility = if(currentgame_btn_fetch.visibility == View.VISIBLE) View.GONE else View.VISIBLE
    }

    private fun showContent() {
        currentgame_progressbar.visibility = View.GONE
        currentgame_rcv.visibility = View.VISIBLE
    }

    private fun subscribe() {
        viewmodel.data?.observe(this@CurrentGameFragment, Observer {
            if(it?.count() == 1){
                adapter.participants = it.first()?.participants
                showContent()
            }
        })

        viewmodel.networkError?.observe(this@CurrentGameFragment, Observer {
            if(it != null){
                toggleVisibility()
                showNetworkErrorDialog(it)
            }
        })
    }


}