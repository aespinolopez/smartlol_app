package com.espino.smartlol.fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.text.Html
import android.text.method.LinkMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.espino.smartlol.R
import kotlinx.android.synthetic.main.fragment_about_smartlol.*


class AboutSmartlolFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about_smartlol, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        about_txv_github.text = Html.fromHtml(getString(R.string.github))
        about_txv_github.movementMethod = LinkMovementMethod.getInstance()
        about_txv_linkedin.text = Html.fromHtml(getString(R.string.linkedin))
        about_txv_linkedin.movementMethod = LinkMovementMethod.getInstance()
    }
}