package com.espino.smartlol.fragments

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.espino.smartlol.R
import com.espino.smartlol.databinding.FragmentChampionTipsContentBinding


class ChampionContentTipsFragment : DialogFragment() {

    private lateinit var binder: FragmentChampionTipsContentBinding

    companion object {
        val TAG = "champion_content_tips_fragment"
        val TITLE = "fragment_title"
        val INDEX = "adapter_index"
        val ENEMY_INDEX = 0
        val ALLY_INDEX = 1
        fun newInstance(args: Bundle): ChampionContentTipsFragment {
            val fragment = ChampionContentTipsFragment()
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binder = DataBindingUtil.inflate(inflater, R.layout.fragment_champion_tips_content, container, false)

        return binder.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val enemyTips = arguments?.getStringArrayList(ChampionBaseTipsFragment.ENEMY_TIPS)
        val allyTips = arguments?.getStringArrayList(ChampionBaseTipsFragment.ALLY_TIPS)

        binder.title = arguments?.getString(TITLE)
        val index = arguments?.getInt(INDEX)
        when(index){
            ENEMY_INDEX -> {
                binder.tips = enemyTips
            }
            ALLY_INDEX -> {
                binder.tips = allyTips
            }
        }

    }
}