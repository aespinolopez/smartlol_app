package com.espino.smartlol.fragments


import android.app.Dialog
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.espino.smartlol.R
import com.espino.smartlol.adapters.ChampionTipsAdapter
import kotlinx.android.synthetic.main.fragment_champion_tips_base.*


class ChampionBaseTipsFragment : DialogFragment() {

    private lateinit var tabLayout: TabLayout
    private lateinit var adapter: ChampionTipsAdapter
//    private lateinit var viewmodel: ChampionTipsViewModel

    companion object {
        val TAG = "champion_tips_fragment"
        val ENEMY_TIPS = "enemy_tips"
        val ALLY_TIPS = "ally_tips"

        fun newInstance(args: Bundle): ChampionBaseTipsFragment {
            val fragment = ChampionBaseTipsFragment()
            fragment.arguments = args

            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v: View? = inflater.inflate(R.layout.fragment_champion_tips_base, container, false)


        return v
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = ChampionTipsAdapter(childFragmentManager, resources.getStringArray(R.array.champion_tips_tabs),
                arguments?.getStringArrayList(ENEMY_TIPS) as ArrayList<String>,
                arguments?.getStringArrayList(ALLY_TIPS) as ArrayList<String>)

        championtips_tablayout.setupWithViewPager(championtips_viewpager)
        championtips_viewpager.adapter = adapter

        tipsdialog_btn_ok.setOnClickListener {
            dismiss()
        }
    }

}