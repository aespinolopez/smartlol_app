package com.espino.smartlol.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.espino.smartlol.R
import kotlinx.android.synthetic.main.fragment_signup_methods.*


class SignUpMethodsFragment : Fragment(){

    interface ISignUpMethod {
        fun onEmailChoose()
        fun onGoogleChoose()
    }

    private lateinit var callback: ISignUpMethod

    companion object {
        val TAG: String = "signup_methods"

        fun newInstance(args: Bundle? = null): SignUpMethodsFragment{
            val fragment = SignUpMethodsFragment()
            fragment.arguments = args

            return fragment
        }
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        try{
            callback = activity as ISignUpMethod
        }catch (ex: ClassCastException){
            Log.e("Activity_error", "activity ${activity!!.javaClass.simpleName} must implement interface SignUpMethodsFragment.ISignUpMethod")
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_signup_methods, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        signupmethod_btn_email.setOnClickListener {
            callback.onEmailChoose()
        }

        signupmethod_btn_google.setOnClickListener {
            callback.onGoogleChoose()
        }
    }
}