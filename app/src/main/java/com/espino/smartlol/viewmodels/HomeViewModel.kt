package com.espino.smartlol.viewmodels


import com.espino.smartlol.models.UserSummonerCredentials
import com.espino.smartlol.models.champion.Champion
import com.espino.smartlol.models.match.CurrentMatch
import com.espino.smartlol.models.summoner.Summoner
import com.espino.smartlol.repositories.UserSummonerCredentialsRepository
import com.espino.smartlol.utils.LiveRealmData


class HomeViewModel : AbstractViewModel<UserSummonerCredentials>(){
    override val repository: UserSummonerCredentialsRepository = UserSummonerCredentialsRepository(dbInstance)
    var oldSummoner = ""
    var oldData: LiveRealmData<UserSummonerCredentials>? = null

    fun updateCredentials(summonerName: String, region: String, userId: Int, oldSummoner: String) {
        this.oldSummoner = oldSummoner
        oldData = data
        data = repository.getData(summonerName, region, userId)
    }

    fun updateLanguage() {
        dbInstance.executeTransactionAsync({
            it.delete(Champion::class.java)
            it.delete(CurrentMatch::class.java)
            it.delete(Summoner::class.java)
        })
    }

}