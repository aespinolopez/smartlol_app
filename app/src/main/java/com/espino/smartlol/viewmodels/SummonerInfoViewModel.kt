package com.espino.smartlol.viewmodels


import com.espino.smartlol.models.summoner.Summoner
import com.espino.smartlol.repositories.SummonerRepository



class SummonerInfoViewModel : AbstractViewModel<Summoner>(){
    override val repository: SummonerRepository = SummonerRepository(dbInstance)

    fun validateSummonerName(name: String): Boolean = name.length in 3..16

}