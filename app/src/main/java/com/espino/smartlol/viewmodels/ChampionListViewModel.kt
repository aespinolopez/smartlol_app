package com.espino.smartlol.viewmodels

import com.espino.smartlol.models.champion.ChampionList
import com.espino.smartlol.repositories.AbstractRepository
import com.espino.smartlol.repositories.ChampionListRepository


class ChampionListViewModel : AbstractViewModel<ChampionList>(){
    override val repository: AbstractRepository<ChampionList> = ChampionListRepository(dbInstance)
}