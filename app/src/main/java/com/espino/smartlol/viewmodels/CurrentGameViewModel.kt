package com.espino.smartlol.viewmodels

import com.espino.smartlol.models.match.CurrentMatch
import com.espino.smartlol.repositories.AbstractRepository
import com.espino.smartlol.repositories.CurrentGameRepository
import io.realm.Realm


class CurrentGameViewModel : AbstractViewModel<CurrentMatch>(){
    override val repository: AbstractRepository<CurrentMatch> = CurrentGameRepository(dbInstance)

    fun getData(): CurrentMatch? {
        return data?.value?.first()
    }
}