package com.espino.smartlol.adapters

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.espino.smartlol.fragments.ChampionBaseTipsFragment
import com.espino.smartlol.fragments.ChampionContentTipsFragment


class ChampionTipsAdapter(fragmentManager: FragmentManager, private var tabs: Array<String>,
                          private var enemyTips: ArrayList<String>,
                          private var allyTips: ArrayList<String>) : FragmentPagerAdapter(fragmentManager) {

    //index of tab elements
    private val ENEMY_TIPS = 0
    private val ALLY_TIPS = 1

    override fun getItem(position: Int): Fragment {
        var fragment: Fragment? = null
        val args = Bundle()
        when(position){
            ENEMY_TIPS -> {
                args.putStringArrayList(ChampionBaseTipsFragment.ENEMY_TIPS, enemyTips)
            }
            ALLY_TIPS -> {
                args.putStringArrayList(ChampionBaseTipsFragment.ALLY_TIPS, allyTips)
            }
        }
        args.putInt(ChampionContentTipsFragment.INDEX, position)
        fragment = ChampionContentTipsFragment.newInstance(args)

        return fragment
    }

    override fun getCount(): Int {
        return tabs.size
    }

    override fun getPageTitle(position: Int): CharSequence {
        return tabs[position]
    }
}