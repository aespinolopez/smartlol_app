package com.espino.smartlol.adapters

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.espino.smartlol.R
import com.espino.smartlol.databinding.ListitemCurrentgameParticipantBinding
import com.espino.smartlol.models.match.Participant
import kotlinx.android.synthetic.main.listitem_currentgame_participant.view.*


class CurrentGameAdapter(private var callback: (participant: Participant) -> Unit) : RecyclerView.Adapter<CurrentGameAdapter.ViewHolder>(){

    var participants: List<Participant>? = null
        set(list) {
            field = list?.sortedBy {
                it.team
            }
        }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(parent?.context)
        val item: ListitemCurrentgameParticipantBinding = DataBindingUtil.inflate(inflater, R.layout.listitem_currentgame_participant, parent, false)

        return ViewHolder(item)
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        holder?.bind(participants?.get(position))
    }

    override fun getItemCount(): Int = participants?.size ?: 0

    inner class ViewHolder(private var participantBinding: ListitemCurrentgameParticipantBinding) : RecyclerView.ViewHolder(participantBinding.root){
        fun bind(participant: Participant?){
            this.participantBinding.participant = participant
            this.participantBinding.participantitemBtnShowrunes.setOnClickListener {
                Log.i("BUTTON SHOW RUNES", "clicked on the button, not the viewholder")
            }
            this.participantBinding.participantitemBtnShowtips.setOnClickListener {
                callback(participant!!)
            }
            this.itemView.setOnClickListener {
                if(itemView.participantitem_linear_expandable.visibility == View.VISIBLE){
                    itemView.participantitem_linear_expandable.visibility = View.GONE
                }else{
                    itemView.participantitem_linear_expandable.visibility = View.VISIBLE
                }
            }


            participantBinding.executePendingBindings()
        }
    }
}