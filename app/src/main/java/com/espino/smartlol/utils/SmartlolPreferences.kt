package com.espino.smartlol.utils

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import java.util.*


class SmartlolPreferences(context: Context) {
    private val FILENAME = "com.espino.smarlol.preferences"
    private val LOGGED_USER_ID = "logged_user_id"
    private val LOGGED_USER_EMAIL = "logged_user_email"
    private val EMAIL_VALIDATED = "email_validated"
    private val RIOT_ID = "riot_id"
    private val RIOT_ACCOUNT_ID = "riot_account_id"
    private val SUMMONER_NAME = "summoner_name"
    private val ACTIVE_REGION = "active_region"
    private val SELECTED_LANGUAGE = "selected_language"

    private val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    var userId: Long
        get() = preferences.getLong(LOGGED_USER_ID, -1L)
        set(value) = preferences.edit().putLong(LOGGED_USER_ID, value).apply()

    var userEmail: String?
        get() = preferences.getString(LOGGED_USER_EMAIL, null)
        set(value) = preferences.edit().putString(LOGGED_USER_EMAIL, value).apply()

    var isValidatedEmail: Boolean
        get() = preferences.getBoolean(EMAIL_VALIDATED, false)
        set(value) = preferences.edit().putBoolean(EMAIL_VALIDATED, value).apply()

    var riotId: Long
        get() = preferences.getLong(RIOT_ID, -1L)
        set(value) = preferences.edit().putLong(RIOT_ID, value).apply()

    var riotAccountId: Long
        get() = preferences.getLong(RIOT_ACCOUNT_ID, -1L)
        set(value) = preferences.edit().putLong(RIOT_ACCOUNT_ID, value).apply()

    var summoner: String?
        get() = preferences.getString(SUMMONER_NAME, null)
        set(value) = preferences.edit().putString(SUMMONER_NAME, value).apply()

    var region: String?
        get() = preferences.getString(ACTIVE_REGION, null)
        set(value) = preferences.edit().putString(ACTIVE_REGION, value).apply()

    var language: Int
        get() = preferences.getString(SELECTED_LANGUAGE, getLanguageId().toString()).toInt()
        set(value) = preferences.edit().putString(SELECTED_LANGUAGE, value.toString()).apply()
}