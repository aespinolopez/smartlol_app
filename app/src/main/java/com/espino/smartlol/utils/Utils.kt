@file:JvmName("Utils")
package com.espino.smartlol.utils

import android.content.Context
import android.databinding.BindingAdapter
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v4.app.Fragment
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import com.espino.smartlol.BR
import com.espino.smartlol.GlideApp
import com.espino.smartlol.R
import com.espino.smartlol.daos.*
import com.espino.smartlol.webservice.NetworkErrorResponse
import io.realm.Realm
import io.realm.RealmObject
import io.realm.RealmResults
import java.util.*

val HTML_TAG_MESSAGE_REGEX = Regex("<p>([\\w\\s@.]+)</p>")

fun Realm.summonerDao() = SummonerDao(this)
fun Realm.championListDao() = ChampionListDao(this)
fun Realm.championDao() = ChampionDao(this)
fun Realm.currentGameDao() = CurrentGameDao(this)
fun Realm.loggedUserDao() = LoggedUserDao(this)
fun Realm.userSummonerCredentialsDao() = UserSummonerCredentialsDao(this)

fun<T: RealmObject> RealmResults<T>.asLiveData() = LiveRealmData(this)

fun Fragment.showNetworkErrorDialog(errorResponse: NetworkErrorResponse){
    var title = ""
    var message = ""
    when(errorResponse.statusCode){
        400->{
            title = "Ha ocurrido un error"
            message = "Ha ocurrido un error inesperado"
        }
        401->{
            title = context?.resources?.getString(R.string.networK_error_response_401_title)!!
            message = context?.resources?.getString(R.string.networK_error_response_401_message)!!
        }
        403->{
            title = context?.resources?.getString(R.string.networK_error_response_403_title)!!
            message = context?.resources?.getString(R.string.networK_error_response_403_message)!!
        }
        404->{
            title = context?.resources?.getString(R.string.networK_error_response_404_title)!!
            message = context?.resources?.getString(R.string.networK_error_response_404_message)!!
        }
        409->{
            title = "Error"
            message = HTML_TAG_MESSAGE_REGEX.find(errorResponse.message!!)!!.groups[1]!!.value
            //group 0 correspond to the entire match, group 1 and so on content between parenthesis
        }
        1->{
            title = context?.resources?.getString(R.string.networK_error_response_1_title)!!
            message = context?.resources?.getString(R.string.networK_error_response_1_message)!!
        }
    }

    if(title != "" && message != ""){
        val dialog: AlertDialog = AlertDialog.Builder(context!!)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(context?.resources?.getString(android.R.string.ok), null)
                .create()

        dialog.show()
    }
}

fun AppCompatActivity.showNetworkErrorDialog(errorResponse: NetworkErrorResponse){
    var title = ""
    var message = ""
    when(errorResponse.statusCode){
        400->{
            title = "BAD REQUEST"
            message = "LA URL O LOS QUERY PARAMS ESTÁN MAL"
        }
        401->{
            title = this.resources?.getString(R.string.networK_error_response_401_title)!!
            message = this.resources?.getString(R.string.networK_error_response_401_message)!!
        }
        403->{
            title =  this.resources?.getString(R.string.networK_error_response_403_title)!!
            message = this.resources?.getString(R.string.networK_error_response_403_message)!!
        }
        404->{
            title = this.resources?.getString(R.string.networK_error_response_404_title)!!
            message = this.resources?.getString(R.string.networK_error_response_404_message)!!
        }
        409->{
            title = "Error"
            message = HTML_TAG_MESSAGE_REGEX.find(errorResponse.message!!)!!.groups[1]!!.value
            //group 0 correspond to the entire match, group 1 and so on content between parenthesis
        }
        1->{
            title = this.resources?.getString(R.string.networK_error_response_1_title)!!
            message = this.resources?.getString(R.string.networK_error_response_1_message)!!
        }
    }

    if(title != "" && message != ""){
        val dialog: AlertDialog = AlertDialog.Builder(this)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(this.resources?.getString(android.R.string.ok), null)
                .create()

        dialog.show()
    }
}

fun Fragment.showActionlessDialog(title: String, message: String){
    val dialog : AlertDialog = AlertDialog.Builder(context!!)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(android.R.string.ok, null)
            .create()

    dialog.show()
}

fun Fragment.getLanguage() : Int{
    val preferences = SmartlolPreferences(context!!)

    return preferences.language
}

fun getLanguageId() : Int{
    return when (Locale.getDefault().language) {
        "en" -> 1
        "es" -> 2
        "it" -> 3
        "fr" -> 4
        "de" -> 5
        else -> 1
    }
}

@BindingAdapter("imageUrl")
fun loadImage(view: ImageView, url: String?){
    GlideApp.with(view.context)
            .load(url)
            .placeholder(R.drawable.ic_icono_loading)
            .error(R.mipmap.ic_launcher)
            .into(view)
}

@BindingAdapter("imageResource")
fun loadImageResource(view: ImageView, resource: String?){
    val deftype = "drawable"
    val resourceId = view.context.resources.getIdentifier(resource?.toLowerCase(), deftype, view.context.packageName)
    view.setImageResource(resourceId)
}


@BindingAdapter("entries", "layout")
fun <T> loadListData(viewGroup: ViewGroup, entries: List<T>?, layoutId: Int){

    if(entries != null && entries.isNotEmpty()){
        viewGroup.removeAllViews()
        val infalter = viewGroup.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        for(entry in entries){
            val binding: ViewDataBinding = DataBindingUtil.inflate(infalter, layoutId, viewGroup, true)
            binding.setVariable(BR.data, entry)
        }
    }
}
