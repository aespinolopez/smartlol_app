package com.espino.smartlol.interfaces

import android.content.Context
import com.espino.smartlol.webservice.NetworkErrorResponse

interface ISignUp {
    interface Presenter {
        fun signUp(username: String, email: String, passwd: String): Boolean
        fun validateUsername(username: String): Boolean
        fun validateEmail(email: String): Boolean
        fun validatePassword(passwd: String): Boolean
        fun checkUsername(username: String)
    }

    interface View {
        fun showValidationError(viewId: Int, message: String)
        fun showNetworkError(error: NetworkErrorResponse)
        fun onSignUp(id: Long, username: String, email: String)
        fun getViewContext(): Context
        fun toggleVisibility()
    }
}