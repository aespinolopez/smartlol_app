package com.espino.smartlol.interfaces

import com.espino.smartlol.models.UserSummonerCredentials
import com.espino.smartlol.webservice.NetworkErrorResponse


interface ISetup {

    interface Presenter {
        fun registerSummoner(summonername: String, region: String, userId: Long)
        fun saveLoggedSummoner(summoner: UserSummonerCredentials)
    }

    interface View {
        fun showNetworkError(error: NetworkErrorResponse)
        fun saveLoggedSummonerPreferences(riotId: Long, riotAccountId: Long, summonername: String)
        fun onSetupComplete()
        fun toggleVisibility()
    }
}