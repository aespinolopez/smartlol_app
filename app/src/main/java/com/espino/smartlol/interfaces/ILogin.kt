package com.espino.smartlol.interfaces

import android.content.Context
import com.espino.smartlol.models.LoggedUser
import com.espino.smartlol.webservice.NetworkErrorResponse


interface ILogin {
    interface Presenter {
        fun authenticate(identifier: String, password: String)
        fun saveLoggedUser(user: LoggedUser)
        fun resendConfirmationEmail(email: String)
        fun sendResetPasswordEmail(email: String)
        fun validateEmail(email: String): Boolean
    }

    interface View {
        fun showNetworkError(error: NetworkErrorResponse)
        fun onAuthenticationSuccess()
        fun saveLoggedState(id: Long, email: String, validated: Boolean)
        fun toggleVisibility()
        fun showValidationError(message: String)
        fun getViewContext(): Context
    }
}